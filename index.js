// Repetition Control Structure (loops)
// Loops are one of the most important feature that programmin must have.
// it lets us execute code repeatedly in a pre-set number or maybe forever.

function greeting() {
  console.log("Hello My World!")

}

// greeting();
// greeting();
// greeting();
// greeting();
// greeting();
// greeting();
// greeting();
// greeting();
// greeting();
// greeting();

// let count = 10;
// while(count !==0) {
//   console.log("This is printed inside the loop " + count);
//   greeting();
//
//   count --;
// }


// [section] while loops
// While loop allow us to repeat an action or an instruction as long as the condition is true.

/*

Syntax:
  while(expression/condition){
  //statement /code block

  finalExpression ++/-- iteration

  -exporession/condition => this are the unit of code that is being evaluated in our loop. loop will run while the condition/expression is true.
  -statement/codeblokc => code/instructions that will be exeecuted several times
  -finalExpression => indicates how to advance the loop.
}


*/
//
// let count = 5;
//
// while(count !== 0){
//   console.log ("While: " +count);
//   //forgetting to include this in loop will make our applcication run an infinite loop which will eventually crash our devices.
//
//   count--;
// }
//



//[SECTION] Do While Loops
// A do-while Loop works a lot like the while loop

/*
  Syntax

    do{
      //statement/codeblock

      finalexpression ++/--
    }while(expression/condition)

  }


*/
//               //number() can also conveert a boolean or date.
// let number = Number(prompt("Give me a number"));
//
// do {
//   console.log("Do While: "+number)
//
//   //Increase the value of number by 1 after every iteration
//   number += 1;
// }
//
// while (number < 10); {
//
// }


//   console.log("While: " +number);
//
//   count++;
// }

// while ()
//

// [Section] For loops

// A for loop is more flexible than while and do-while loops.


/*
  Syntax:
    for(initialization; expression/condition; finalExpression ++/--) {
    //Codeblock / Statement
  }

    Three parts of the for loop:
    1. Initialization => value that will track the progression of the loops
    2. expression/condition => this will be evaluated to determine if the loop will run one more time.
    3. finalExpression => inidicates how the loop advances.

*/
/*




*/
// [Sample Code]
// for(let count = 0;count<=20;count+=2) {
//   console.log("For Loop: " +count);
//
//     }
//
//

/*
  let myString ="angelito"

  expected outpu:
  a
  n
  g
  e
  l
  i
  t
  o


*/


// Loop using string property

// let myString = "Jake";
//
// // .length property - used to count the number of characters in a string.
// //
// // console.log(myString.length);
// //
// // // Accessing element of a string
// // // individual characters of a string may be accessed using it's index number.
// // // The first chracter in string corresponds to the number -, to the nth number.
// //
// // console.log(myString[0]);
// // console.log(myString[3]);
// // console.log(myString[myString.length-1]);
//
// // Create a loop that will display the individual letters of the myString variable in the console
//
// for(let x = 0;x < myString.length;x++){
//   console.log(myString[x]);
// }



/*
  creates aloop that will print out the letters of the name individually and print out the number 3 instead when the letter to be out is a vowel


*/

// let myName = "Tolits";
// let myNewName = ""
//
// for (let i = 0; i < myName.length; i++) {
//   if (myName[i].toLowerCase() == "a" ||
//     myName[i].toLowerCase() == "e" ||
//     myName[i].toLowerCase() == "i" ||
//     myName[i].toLowerCase() == "o" ||
//     myName[i].toLowerCase() == "u") {
//     console.log(3)
//
//     myNewName += 3;
//   } else {
//     console.log(myName[i]);
//     myNewName += myName[i]
//   }
// }
//
// console.log(myNewName)

/*
  "continue" statement allows the code to go to the next itreration of the loop without finishing the execution of all the code block.
  -"break" statement is used to terminate the current loop once a match has been found.

  create a loop that if the count values is divded by 2 and the remainder is 0, it will not print the number and cotinue to the next iteration of the loop. if the current count value is greater than 10 it will stop the loop.




*/

// for (let count = 0; count <= 20; count++) {
//   // console.log(count);
//   if (count % 2 === 0) {
//     continue;
//   }
//   console.log("Continue and break: " + count);
//
//   if (count > 10){
//     break;
//   }
// }


/*
 Mini Actiivty

 Creates a loop that will iterate based on the length of the string. if the current letter is equivalent to "a", print "continue to the next iteration" and skip the current letter. If the current letter is equal to "D" stop the loop.

 let name = "alexandro";


*/

let name = "alexandro";

for (let z = 0; z < name.length; z++)
  if (name[z] == "a") {
    console.log("continue to the next iteration");
    continue;

  }
else if (name[z] == "d") {
  console.log(name[z]);
  break;

} else {
  console.log(name[z]);
}





//
